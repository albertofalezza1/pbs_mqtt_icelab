import networkx as nx
import matplotlib.pyplot as plt
import yaml


def InitializeGraph():
    edge_list = []
    with open(r'graph_fleet.yaml') as file:
        file = yaml.load(file, Loader=yaml.FullLoader)
        list_of_edges = file["edges"]
        for edge in list_of_edges:
            new_val = edge.replace("-", ", ")
            edge_list.append(tuple(map(int, new_val.split(', '))))

    # list_of_edges = [(0, 1), (1, 2), (2, 3), (1, 3), (1, 4), (4, 0), (5, 1), (1, 6), (6, 7), (7, 8), (8, 9), (9, 8),
    #                  (8, 7), (7, 6), (6, 1)]
    G = nx.Graph(edge_list)
    # G = nx.Graph()
    # options = {
    #     "font_size": 36,
    #     "node_size": 3000,
    #     "node_color": "white",
    #     "edgecolors": "black",
    #     "linewidths": 5,
    #     "width": 5,
    # }
    # pos = {1: (0, 0), 2: (1, -1), 3: (2, 0), 4: (2, 1), 5: (3, 1), 6: (3, 0), 7: (4, 0), 8: (4, 1), 9: (5, 0),
    #        10: (5, 1), 11: (6, 0), 12: (6, 1), 13: (7, 0), 14: (7, 1), 15: (8, 1), 16: (8, 0)}
    #
    # G.add_edge(1, 2)
    # G.add_edge(1, 3)
    # G.add_edge(2, 3)
    # G.add_edge(3, 4)
    # G.add_edge(4, 5)
    # G.add_edge(5, 6)
    # G.add_edge(3, 6)
    # G.add_edge(6, 7)
    # G.add_edge(7, 8)
    # G.add_edge(7, 9)
    # G.add_edge(9, 10)
    # G.add_edge(9, 11)
    # G.add_edge(11, 12)
    # G.add_edge(11, 13)
    # G.add_edge(13, 14)
    # G.add_edge(14, 15)
    # G.add_edge(15, 16)
    # G.add_edge(13, 16)

    # nx.draw_networkx(G, pos, options)
    # Set margins for the axes so that nodes aren't clipped
    # ax = plt.gca()
    # ax.margins(0.20)
    # plt.axis("off")
    # plt.show()

    return G


def astar_path(G, start, end):
    # def dist(a, b):  # a, b
    # (x1, y1) = a
    # (x2, y2) = b
    # return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5

    # return nx.astar_path(G, start, end, heuristic=dist, weight='cost')
    return nx.astar_path(G, start, end, weight='cost')


def create_visual_frame(graph, agents):
    if len(agents[0].path) != len(agents[1].path):
        max_l = max(len(agents[0].path), len(agents[1].path))
        for i in range(len(agents[0].path), max_l):
            agents[0].path.append(agents[0].path[-1])
        for i in range(len(agents[1].path), max_l):
            agents[1].path.append(agents[1].path[-1])

    import os
    import glob
    files = glob.glob(f'./img/*')
    for f in files:
        os.remove(f)

    pos = {1: (0, 0), 2: (1, -1), 3: (2, 0), 4: (2, 1), 5: (3, 1), 6: (3, 0), 7: (4, 0), 8: (4, 1), 9: (5, 0),
           10: (5, 1), 11: (6, 0), 12: (6, 1), 13: (7, 0), 14: (7, 1), 15: (8, 1), 16: (8, 0)}

    color_map = ["white"] * len(list(graph))
    for i, (node1, node2) in enumerate(zip(agents[0].path, agents[1].path)):
        index1 = list(graph).index(node1)
        index2 = list(graph).index(node2)
        color_map[index1] = "cyan"
        color_map[index2] = "yellow"
        nx.draw(graph, pos, node_color=color_map, with_labels=True)
        ax = plt.gca()
        ax.margins(0.20)
        plt.axis("off")
        plt.savefig(f'./img/img_{i}.png',
                    transparent=False,
                    facecolor='white'
                    )
        color_map[index1] = "white"
        color_map[index2] = "white"
        plt.close()

    from PIL import Image
    frame_folder = './img'
    frames = [Image.open(image) for image in glob.glob(f"{frame_folder}/*.PNG")]
    frame_one = frames[0]
    frame_one.save(f"{frame_folder}/complete_path.gif", format="GIF", append_images=frames, save_all=True, duration=500,
                   loop=0)
