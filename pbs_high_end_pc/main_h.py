import pbs_high_end_pc.create_graph_h as cg
import pbs_high_end_pc.priority_tree_h as pt
import copy
import multiprocessing as mp
from pbs_high_end_pc.agent_h import Agent
from publisher import pub_run


def start(start_position_agents):
    # create the graph as Ice lab
    graph = cg.InitializeGraph()
    agents = []

    number_of_agents = 2
    speed_of_agents = [1.5, 1.5]  # suppose velocity as m/s
    # start_position_agents = [8, 12]  # general starting position of each agent
    # a - b
    FIFO_destinations = [1, 4]  # destinations of our agents

    queue_path = mp.Queue()

    for i in range(number_of_agents):
        agents.append(Agent(i, start_position_agents[i], speed_of_agents[i]))

    # initialize destinations
    for i, des in enumerate(FIFO_destinations):
        agents[i].destination = des

    for agent in agents:
        agent.path = cg.astar_path(graph, agent.position, agent.destination)

    pt.set_collision_if_exist(number_of_agents, agents)
    collision = pt.return_collision(agents)
    # if collision is False:
    #     for agent in agents:
    #         print(vars(agent))
    # else:
    pt.tree_creation(copy.deepcopy(agents), graph, queue_path)

    while True:
        if collision is False:
            break
        if pt.close_threads is True:
            for pc in pt.threads:
                pc.join()
            break
    pt.close_threads = False

    if collision is True:
        agents = queue_path.get()
    if len(agents[0].path) == 1 and len(agents[1].path) == 1:
        return None
    else:
        pub_run(agents)
        return agents

    # cg.create_visual_frame(graph, copy.deepcopy(agents))
