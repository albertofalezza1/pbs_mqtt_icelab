
class Agent:

    def __init__(self, id, start_position, goal_position):
        self.ID = id
        self.priority = 0
        self.speed = 0
        self.position = start_position
        self.path = []
        self.destination = goal_position

        self.collision_ID = 0
        self.collision_node = 0
        self.collision_pos = 0
        self.collision = False

        self.already_checked = False

    def same_agent(self, other):
        return self.ID == other
