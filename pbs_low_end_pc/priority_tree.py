from itertools import permutations
import threading as t
from create_graph import astar_path
import copy
import random

threads = []
close_threads = False
lock = t.Lock()


def set_collision_if_exist(number_of_agents, agents):
    collision = False
    agent_confronted_buffer = []
    for i in range(number_of_agents):
        for j in range(number_of_agents):
            tup = sorted((agents[i].ID, agents[j].ID))
            if i != j and tup not in agent_confronted_buffer:
                agent_confronted_buffer.append(
                    sorted((agents[i].ID, agents[j].ID)))  # add to the buffer the path I have confronted
                collision = search_for_collision(agents[i], agents[j])
                if collision is True:
                    break
        if collision is True:
            break


def search_for_collision(agent_1, agent_2):
    agent_1_path, agent_2_path = agent_1.path, agent_2.path
    max_length = max(len(agent_1_path), len(agent_2_path))
    for i in range(max_length):
        if i < len(agent_1_path) and i < len(agent_2_path):
            if agent_1_path[i] == agent_2_path[i]:
                agent_1.collision_ID = agent_2.ID
                agent_2.collision_ID = agent_1.ID
                agent_1.collision_node = agent_2_path[i]
                agent_2.collision_node = agent_1_path[i]
                agent_1.collision_pos = i
                agent_2.collision_pos = i
                agent_1.collision = True
                agent_2.collision = True
                return True
            else:
                agent_1.collision = False
                agent_2.collision = False
        if i < len(agent_1_path) - 1 and i < len(agent_2_path) - 1:
            if agent_1_path[i] == agent_2_path[i + 1] and agent_1_path[i + 1] == agent_2_path[i]:
                agent_1.collision_ID = agent_2.ID
                agent_2.collision_ID = agent_1.ID
                agent_1.collision_node = agent_2_path[i]  # agent_2_path[i] if we want + 1
                agent_2.collision_node = agent_1_path[i]  # agent_1_path[i] if we want + 1
                agent_1.collision_pos = i + 1
                agent_2.collision_pos = i + 1
                agent_1.collision = True
                agent_2.collision = True
                return True
            else:
                agent_1.collision = False
                agent_2.collision = False
        if i >= len(agent_1_path):
            if agent_2_path[i] == agent_1_path[-1]:
                agent_1.collision_ID = agent_2.ID
                agent_2.collision_ID = agent_1.ID
                agent_1.collision_node = agent_2_path[i]
                agent_2.collision_node = agent_1_path[-1]
                agent_1.collision_pos = len(agent_1_path) - 1
                agent_2.collision_pos = i
                agent_1.collision = True
                agent_2.collision = True
                return True
            else:
                agent_1.collision = False
                agent_2.collision = False
        if i >= len(agent_2_path):
            if agent_1_path[i] == agent_2_path[-1]:
                agent_1.collision_ID = agent_2.ID
                agent_2.collision_ID = agent_1.ID
                agent_1.collision_node = agent_2_path[-1]
                agent_2.collision_node = agent_1_path[i]
                agent_1.collision_pos = i
                agent_2.collision_pos = len(agent_2_path) - 1
                agent_1.collision = True
                agent_2.collision = True
                return True
            else:
                agent_1.collision = False
                agent_2.collision = False
    return False


def return_collision(agents):
    for agent in agents:
        if agent.collision is True:
            return True
    return False


def tree_creation(agents, graph, queue_path):
    global close_threads

    if close_threads is True:
        return

    for agent in agents:
        if not agent.already_checked:
            priority = []
            priority_set = []
            list_of_collision_agents = [agent.collision_ID]
            list_of_collision_agents.insert(0, agent.ID)

            agent.already_checked = True
            other_agent = [x for x in agents if x.ID == agent.collision_ID].pop()
            other_agent.already_checked = True

            for i in range(len(list_of_collision_agents)):
                priority.append(i)
            for subset in permutations(priority, len(priority)):
                priority_set.append(list(subset))
            random.shuffle(priority_set)
            for p in priority_set:
                if close_threads is False:
                    thread_management(copy.deepcopy(agents), list_of_collision_agents, graph, p, queue_path)


def thread_management(agents, list_of_collision_agents, graph, p, queue_path):
    global threads

    pc = t.Thread(target=collision_pbs_resolve, args=(copy.deepcopy(agents), list_of_collision_agents, graph, p, queue_path))
    threads.append(pc)
    pc.daemon = True
    pc.start()


def collision_pbs_resolve(agents, list_of_collision_id, graph, priority, queue_path):
    agent_list_to_compare = []
    for agent in agents:
        for collision_id in list_of_collision_id:
            if agent.same_agent(collision_id):
                agent.priority = priority.pop()
                agent_list_to_compare.append(agent)

    for agent1 in agent_list_to_compare:
        for agent2 in agent_list_to_compare:
            if agent1.ID == agent2.ID:
                continue
            else:
                if agent1.priority < agent2.priority:
                    # collision_node = agent1.path.pop(agent1.collision_pos)
                    # new_graph = copy.deepcopy(graph)
                    # new_graph.remove_node(agent1.collision_node)
                    # search_new_path(graph, new_graph, agent1, agent2, collision_node, agents, queue_path)
                    search_new_path2(graph, agent1, agent2, agents, queue_path)


def search_new_path2(graph, agent1, agent2, agents, queue_path):
    global close_threads

    if close_threads is True or agent1.collision_pos == 0:
        return

    set_collision_if_exist(len(agents), agents)
    if return_collision(agents) is True:
        # lock.acquire()
        # for agent in agents:
        #     print(vars(agent))
        # lock.release()
        agent1.already_checked = False
        agent2.already_checked = False

        if len(agent1.path) - 1 == agent1.collision_pos and len(agent1.path) != len(agent2.path):
            list_neighbors = [n for n in graph.neighbors(agent1.path[-1])]
            for node in list_neighbors:
                start = node
                goal = agent1.path[-1]
                pc = t.Thread(target=new_path_thread, args=(graph, copy.deepcopy(agent1), copy.deepcopy(agents), queue_path, start, goal))
                threads.append(pc)
                pc.daemon = True
                pc.start()
        else:
            agent1.path = agent1.path[:agent1.collision_pos]
            list_neighbors = [n for n in graph.neighbors(agent1.path[-1])]
            list_neighbors.append(agent1.path[-1])
            list_neighbors_filtered = [x for x in list_neighbors if x not in agent2.path[:agent2.collision_pos]]
            for node in list_neighbors_filtered:
                start = node
                goal = agent1.destination
                pc = t.Thread(target=new_path_thread, args=(graph, copy.deepcopy(agent1), copy.deepcopy(agents), queue_path, start, goal))
                threads.append(pc)
                pc.daemon = True
                pc.start()
    else:
        lock.acquire()
        if close_threads is False:
            #for agent in agents:
            #   print(vars(agent))
            queue_path.put(agents)
            close_threads = True
            for agent in agents:
                agent.reset_value()
        lock.release()


def new_path_thread(graph, agent1, agents, queue_path, start, goal):
    new_path = astar_path(graph, start, goal)  # new thread possibility
    for pos in new_path:
        agent1.path.append(pos)
    for i, agent in enumerate(agents):
        if agent.ID == agent1.ID:
            agents[i] = agent1
    tree_creation(copy.deepcopy(agents), graph, queue_path)
