import networkx as nx
import matplotlib.pyplot as plt


def InitializeGraph():
    G = nx.Graph()
    options = {
        "font_size": 36,
        "node_size": 3000,
        "node_color": "white",
        "edgecolors": "black",
        "linewidths": 5,
        "width": 5,
    }
    pos = {1: (0, 0), 2: (1, -1), 3: (2, 0), 4: (2, 1), 5: (3, 1), 6: (3, 0), 7: (4, 0), 8: (4, 1), 9: (5, 0),
           10: (5, 1), 11: (6, 0), 12: (6, 1), 13: (7, 0), 14: (7, 1), 15: (8, 1), 16: (8, 0)}

    G.add_edge(1, 2)
    G.add_edge(1, 3)
    G.add_edge(2, 3)
    G.add_edge(3, 4)
    G.add_edge(4, 5)
    G.add_edge(5, 6)
    G.add_edge(3, 6)
    G.add_edge(6, 7)
    G.add_edge(7, 8)
    G.add_edge(7, 9)
    G.add_edge(9, 10)
    G.add_edge(9, 11)
    G.add_edge(11, 12)
    G.add_edge(11, 13)
    G.add_edge(13, 14)
    G.add_edge(14, 15)
    G.add_edge(15, 16)
    G.add_edge(13, 16)

    # nx.draw_networkx(G, pos, options)
    # Set margins for the axes so that nodes aren't clipped
    # ax = plt.gca()
    # ax.margins(0.20)
    # plt.axis("off")
    # plt.show()

    return G


def astar_path(G, start, end):
    def dist(a, b):
        x1 = a
        x2 = b
        return 1

    return nx.astar_path(G, start, end, heuristic=dist, weight='weight')


def create_visual_frame(graph, agents):
    if len(agents[0].path) != len(agents[1].path):
        max_l = max(len(agents[0].path), len(agents[1].path))
        for i in range(len(agents[0].path), max_l):
            agents[0].path.append(agents[0].path[-1])
        for i in range(len(agents[1].path), max_l):
            agents[1].path.append(agents[1].path[-1])

    import os
    import glob
    files = glob.glob(f'./img/*')
    for f in files:
        os.remove(f)

    pos = {1: (0, 0), 2: (1, -1), 3: (2, 0), 4: (2, 1), 5: (3, 1), 6: (3, 0), 7: (4, 0), 8: (4, 1), 9: (5, 0),
           10: (5, 1), 11: (6, 0), 12: (6, 1), 13: (7, 0), 14: (7, 1), 15: (8, 1), 16: (8, 0)}

    color_map = ["white"] * len(list(graph))
    for i, (node1, node2) in enumerate(zip(agents[0].path, agents[1].path)):
        index1 = list(graph).index(node1)
        index2 = list(graph).index(node2)
        color_map[index1] = "cyan"
        color_map[index2] = "yellow"
        nx.draw(graph, pos, node_color=color_map, with_labels=True)
        ax = plt.gca()
        ax.margins(0.20)
        plt.axis("off")
        plt.savefig(f'./img/img_{i}.png',
                    transparent=False,
                    facecolor='white'
                    )
        color_map[index1] = "white"
        color_map[index2] = "white"
        plt.close()

    from PIL import Image
    frame_folder = './img'
    frames = [Image.open(image) for image in glob.glob(f"{frame_folder}/*.PNG")]
    frame_one = frames[0]
    frame_one.save(f"{frame_folder}/complete_path.gif", format="GIF", append_images=frames, save_all=True, duration=500, loop=0)

def create_visual_frame_hardcoded(graph, agents):
    max_l = 0
    if len(agents[0].path) != len(agents[1].path):
        max_l = max(len(agents[0].path), len(agents[1].path), len(agents[2].path), len(agents[3].path), len(agents[4].path), len(agents[5].path))
        for i in range(len(agents[0].path), max_l):
            agents[0].path.append(agents[0].path[-1])
        for i in range(len(agents[1].path), max_l):
            agents[1].path.append(agents[1].path[-1])
        for i in range(len(agents[2].path), max_l):
            agents[2].path.append(agents[2].path[-1])
        for i in range(len(agents[3].path), max_l):
            agents[3].path.append(agents[3].path[-1])
        for i in range(len(agents[4].path), max_l):
            agents[4].path.append(agents[4].path[-1])
        for i in range(len(agents[5].path), max_l):
            agents[5].path.append(agents[5].path[-1])

    import os
    import glob
    files = glob.glob(f'./img/*')
    for f in files:
        os.remove(f)

    pos = {1: (0, 0), 2: (1, -1), 3: (2, 0), 4: (2, 1), 5: (3, 1), 6: (3, 0), 7: (4, 0), 8: (4, 1), 9: (5, 0),
           10: (5, 1), 11: (6, 0), 12: (6, 1), 13: (7, 0), 14: (7, 1), 15: (8, 1), 16: (8, 0)}

    color_map = ["white"] * len(list(graph))
    for i in range(max_l):
        index1 = list(graph).index(agents[0].path[i])
        index2 = list(graph).index(agents[1].path[i])
        index3 = list(graph).index(agents[2].path[i])
        index4 = list(graph).index(agents[3].path[i])
        index5 = list(graph).index(agents[4].path[i])
        index6 = list(graph).index(agents[5].path[i])
        color_map[index1] = "cyan"
        color_map[index2] = "yellow"
        color_map[index3] = "green"
        color_map[index4] = "purple"
        color_map[index5] = "red"
        color_map[index6] = "gray"
        nx.draw(graph, pos, node_color=color_map, with_labels=True)
        ax = plt.gca()
        ax.margins(0.20)
        plt.axis("off")
        plt.savefig(f'./img/img_{i}.png',
                    transparent=False,
                    facecolor='white'
                    )
        color_map[index1] = "white"
        color_map[index2] = "white"
        color_map[index3] = "white"
        color_map[index4] = "white"
        color_map[index5] = "white"
        color_map[index6] = "white"
        plt.close()

    from PIL import Image
    frame_folder = './img'
    frames = [Image.open(image) for image in glob.glob(f"{frame_folder}/*.PNG")]
    frame_one = frames[0]
    frame_one.save(f"{frame_folder}/complete_path.gif", format="GIF", append_images=frames, save_all=True, duration=1000, loop=0)
