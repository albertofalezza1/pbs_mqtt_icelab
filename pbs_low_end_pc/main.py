import create_graph as cg
import priority_tree as pt
import copy
import multiprocessing as mp
from agent import Agent

if __name__ == '__main__':
    # create the graph as Ice lab
    check_queue = False
    graph = cg.InitializeGraph()
    agents = []
    new_agents = []

    number_of_agents = 6
    speed_of_agents = [1.5, 1.5, 1.5, 1.5, 1.5, 1.5]  # suppose velocity as m/s
    start_position_agents = [10, 7, 11, 4, 13, 2]  # general starting position of each agent
    FIFO_destinations = [14, 12, 8, 16, 5, 3]  # destinations of our agents

    queue_path = mp.Queue()

    for i in range(number_of_agents):
        agents.append(Agent(i, start_position_agents[i], speed_of_agents[i]))

    # initialize destinations
    for i, des in enumerate(FIFO_destinations):
        agents[i].destination = des

    for agent in agents:
        agent.path = cg.astar_path(graph, agent.position, agent.destination)

    for agent in agents:
        print(vars(agent))

    while True:
        pt.set_collision_if_exist(number_of_agents, agents)
        collision = pt.return_collision(agents)
        if collision is False:
            break
        else:
            collision_agents = []
            for agent in agents:
                if agent.collision is True:
                    collision_agents.append(agent)
            pt.tree_creation(copy.deepcopy(collision_agents), graph, queue_path)

            if pt.close_threads is True:
                new_agents = queue_path.get()
                for pc in pt.threads:
                    pc.join()
                pt.threads.clear()
            pt.close_threads = False

            for new_agent in new_agents:
                for i, agent in enumerate(agents):
                    if agent.ID == new_agent.ID:
                        agents[i] = new_agent

            #for agent in agents:
                #print(vars(agent))

    # while True:
    #   if collision is False:
    #        break
    #    if pt.close_threads is True:
    #        for pc in pt.threads:
    #            pc.join()
    #        break

    for agent in agents:
        print(vars(agent))

    cg.create_visual_frame_hardcoded(graph, copy.deepcopy(agents))
