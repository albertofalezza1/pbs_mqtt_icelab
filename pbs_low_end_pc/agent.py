
class Agent:

    def __init__(self, id, start_position, speed):
        self.ID = id
        self.priority = 0
        self.speed = speed
        self.position = start_position
        self.path = []
        self.destination = 0

        self.collision_ID = None
        self.collision_node = None
        self.collision_pos = None
        self.collision = False

        self.already_checked = False

    def same_agent(self, other):
        return self.ID == other

    def reset_value(self):
        self.priority = 0
        self.collision_ID = None
        self.collision_node = None
        self.collision_pos = None
        self.collision = False
        self.already_checked = False
