import paho.mqtt.client as mqtt
import json
from pbs_high_end_pc.main_h import start

host = "10.197.21.10"
port = 31883
topic = "ice/json/rbkairos/+/cmd/current_waypoint"

positions = [-1, -1]
first_robot_msg = False
second_robot_msg = False


def init():
    def connect_mqtt():
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt.Client("pub")
        client.on_connect = on_connect
        client.connect(host, port)
        return client

    def publish(client):
        start_msg = -1

        topic_start_a = "ice/json/rbkairos/a/cmd/navigation"
        topic_start_b = "ice/json/rbkairos/b/cmd/navigation"
        msg = json.dumps(start_msg)

        result_a = client.publish(topic_start_a, msg)
        result_b = client.publish(topic_start_b, msg)
        # result: [0, 1]
        status_a = result_a[0]
        status_b = result_b[0]
        if status_a == 0 and status_b == 0:
            print(f"Send `{msg}` to topic `{topic_start_a}`")
            print(f"Send `{msg}` to topic `{topic_start_b}`")
        else:
            print(f"Failed to send message to topic {topic_start_a} or to topic {topic_start_b}")

    client = connect_mqtt()
    client.loop_start()
    publish(client)
    client.disconnect()


def run():
    global positions, first_robot_msg, second_robot_msg

    def on_connect(client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        client.subscribe(topic)

    def on_message(client, userdata, msg):
        global positions, first_robot_msg, second_robot_msg
        print("Received " + msg.payload.decode() + " from " + msg.topic + " topic.")
        if msg.topic == "ice/json/rbkairos/a/cmd/current_waypoint":
            data = json.loads(msg.payload.decode())
            # positions[0] = int(data["position"])
            positions[0] = int(data)
            first_robot_msg = True
            if first_robot_msg and second_robot_msg:
                first_robot_msg = False
                second_robot_msg = False
                # print("Start")
                agents = start(positions)
                if agents is None:
                    client.disconnect()
                # else:
                #    for agent in agents:
                #        print(vars(agent))
        elif msg.topic == "ice/json/rbkairos/b/cmd/current_waypoint":
            data = json.loads(msg.payload.decode())
            # positions[1] = int(data["position"])
            positions[1] = int(data)

            # positions[0] = 3
            # first_robot_msg = True

            second_robot_msg = True
            if first_robot_msg and second_robot_msg:
                first_robot_msg = False
                second_robot_msg = False
                # print("Start")
                agents = start(positions)
                if agents is None:
                    client.disconnect()
                # else:
                #     for agent in agents:
                #         print(vars(agent))
        else:
            print("Error!")

    client_mqtt = mqtt.Client()
    client_mqtt.on_connect = on_connect
    client_mqtt.on_message = on_message
    client_mqtt.connect(host, port)
    init()
    client_mqtt.loop_forever()


if __name__ == '__main__':
    run()
