import paho.mqtt.client as mqtt
import time
import json

host = "10.197.21.10"
port = 31883


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt.Client("pub")
    client.on_connect = on_connect
    client.connect(host, port)
    return client


def publish(client, agents):
    # time.sleep(1)
    for agent in agents:
        if len(agent.path) > 1:
            agent.path.pop(0)
        if agent.ID == 0:
            # data = {
            #     "position": agent.path[0],
            #     "path": agent.path,
            #     "destination": agent.destination
            # }
            data = agent.path[0]
            msg = json.dumps(data)
            topic = "ice/json/rbkairos/a/cmd/navigation"
        else:
            # data = {
            #     "position": agent.path[0],
            #     "path": agent.path,
            #     "destination": agent.destination
            # }
            data = agent.path[0]
            topic = "ice/json/rbkairos/b/cmd/navigation"
            msg = json.dumps(data)

        result = client.publish(topic, msg)
        print("result publish  " + str(result))
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")


def pub_run(agents):
    client = connect_mqtt()
    client.loop_start()
    publish(client, agents)
    time.sleep(1)
    client.disconnect()
